$(document).ready(
	function() {
	
	$(document).mousemove(function(e){
		var $width = ($(document).width())/155;
		var $height = ($(document).height())/155;
		var $pageX = (parseInt(e.pageX / $width,10))+100;
		var $pageY = (parseInt(e.pageY / $height,10))+100;
		$("#box1, #box2, #box3").css("background-color", "rgb("+$pageY+","+$pageX+",220)");
		$("a").css("color", "rgb("+$pageX+","+$pageX+",220)");
		
	}); 
	
	$("#box1").mouseout(function() {
		$(this).css({
			"background-color" : "rgba(0,0,0,.8)",
			"background" : "-moz-linear-gradient(left,  rgba(30,87,153,.5) 0%, rgba(125,185,232,0) 100%)", /* FF3.6+ */
			"background" : "-webkit-gradient(linear, left top, right top, color-stop(0%,rgba(30,87,153,.5)), color-stop(100%,rgba(125,185,232,0)))", /* Chrome,Safari4+ */
			"background" : "-webkit-linear-gradient(left,  rgba(30,87,153,.5) 0%,rgba(125,185,232,0) 100%)", /* Chrome10+,Safari5.1+ */
			"background" : "-o-linear-gradient(left,  rgba(30,87,153,.5) 0%,rgba(125,185,232,0) 100%)", /* Opera 11.10+ */
			"background" : "-ms-linear-gradient(left,  rgba(30,87,153,.5) 0%,rgba(125,185,232,0) 100%)", /* IE10+ */
			"background" : "linear-gradient(to right,  rgba(30,87,153,.5) 0%,rgba(125,185,232,0) 100%)", /* W3C */
			"filter" : "progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#007db9e8',GradientType=1 )" /* IE6-9 */
		});
	});
	$("#box2").mouseout(function() {
		$(this).css({
			"background" : "-moz-linear-gradient(left,  rgba(191,38,38,.5) 0%, rgba(125,185,232,0) 100%)", /* FF3.6+ */
			"background": "-webkit-gradient(linear, left top, right top, color-stop(0%,rgba(191,38,38,.5)), color-stop(100%,rgba(125,185,232,0)))", /* Chrome,Safari4+ */
			"background": "-webkit-linear-gradient(left,  rgba(191,38,38,.5) 0%,rgba(125,185,232,0) 100%)", /* Chrome10+,Safari5.1+ */
			"background": "-o-linear-gradient(left,  rgba(191,38,38,.5) 0%,rgba(125,185,232,0) 100%)", /* Opera 11.10+ */
			"background": "-ms-linear-gradient(left,  rgba(191,38,38,.5) 0%,rgba(125,185,232,0) 100%)", /* IE10+ */
			"background": "linear-gradient(to right,  rgba(191,38,38,.5) 0%,rgba(125,185,232,0) 100%)", /* W3C */
			"filter" : "progid:DXImageTransform.Microsoft.gradient( startColorstr='#bf2626', endColorstr='#007db9e8',GradientType=1 )" /* IE6-9 */
		});
	});
	$("#box3").mouseout(function() {
		$(this).css({
			"background" : "-moz-linear-gradient(left,  rgba(168,178,35,.5) 0%, rgba(125,185,232,0) 100%)", /* FF3.6+ */
			"background" : "-webkit-gradient(linear, left top, right top, color-stop(0%,rgba(168,178,35,.5)), color-stop(100%,rgba(125,185,232,0)))", /* Chrome,Safari4+ */
			"background" : "-webkit-linear-gradient(left,  rgba(168,178,35,.5) 0%,rgba(125,185,232,0) 100%)", /* Chrome10+,Safari5.1+ */
			"background" : "-o-linear-gradient(left,  rgba(168,178,35,.5) 0%,rgba(125,185,232,0) 100%)", /* Opera 11.10+ */
			"background" : "-ms-linear-gradient(left,  rgba(168,178,35,.5) 0%,rgba(125,185,232,0) 100%)",			/* IE10+ */
			"background" : "linear-gradient(to right,  rgba(168,178,35,.5) 0%,rgba(125,185,232,0) 100%)", /* W3C */
			"filter" : "progid:DXImageTransform.Microsoft.gradient( startColorstr='#a8b223', endColorstr='#007db9e8',GradientType=1 )" /* IE6-9 */
		});
	});
	
	$("#box1").click(function () {
		var $this = $(this),
		val = 505;
		$this.stop().animate({
			width: '100%',
			height: val
		}, "slow");
		$("#cat_content1").stop().show("slow");
		$("#up1").stop().show("slow");
	});
	
	$("#up1").click(function (e) {
		e.stopPropagation();
		$("#cat_content1").stop().hide("slow");
		$("#up1").stop().hide("slow");
		val = 100;
		$("#box1").stop().animate({
			width: '100%',
			height: val
		}, "slow");
	});
	
	$("#box2").click(function () {
		var $this = $(this),
		val = 445;
		$this.stop().animate({
			width: '100%',
			height: val
		}, "slow");
		$("#cat_content2").stop().show("slow");
		$("#up2").stop().show("slow");
	});
	
	$("#up2").click(function (e) {
		e.stopPropagation();
		$("#cat_content2").stop().hide("slow");
		$("#up2").stop().hide("slow");
		val = 100;
		$("#box2").stop().animate({
			width: '100%',
			height: val
		}, "slow");
	});
	
	$("#box3").click(function () {
		var $this = $(this),
		val = 745;
		$this.stop().animate({
			width: '100%',
			height: val
		}, "slow");
		$("#cat_content3").stop().show("slow");
		$("#up3").stop().show("slow");
	});
	
	$("#up3").click(function (e) {
		e.stopPropagation();
		$("#cat_content3").stop().hide("slow");
		$("#up3").stop().hide("slow");
		val = 100;
		$("#box3").stop().animate({
			width: '100%',
			height: val
		}, "slow");
	});
	
	
	
//	$("#email").hover(function(){
//		$(this).stop().animate({ color: '#000000'}, "slow");
//	}, function() {
//		$(this).stop().animate({ color: '#ffffff'}, "slow"); //original color
//	});
	
});